# To-Do List
Please create a web page that shows a list of items that can be selected as done or not.  At the very minimum, save the entered items into local storage in the browser.  If you wish to get your hands a bit more dirty then you could also research how to store content online in Firebase, although that is not specifically required.

Have two buttons on the page that allow either all items to be shown, or only ones that are not yet checked.

Please reference the index.html file to get an idea of how to start.

Good luck!